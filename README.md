# Tourney Platform - NestJS

## Description
Tourney is a platform comprising multiple microservices tailored for organizing and managing e-sports tournaments within local gaming communities.
The project is built using the [Nest.js](https://docs.nestjs.com) framework in a monorepo configuration. 

### Apps
The Tourney platform monorepo comprises multiple microservices within the `apps/` directory:

- ***[Core Service:](apps/core-service/README.md):*** Core mechanisms and logic of the tourney platform.
- ***[Gateway Service](apps/gateway-service/README.md):*** Public facing service with an API to interact with the various services.
- ***[Logger Service](apps/logger-service/README.md):*** Service for logging events from all microservices.
- ***[User Service](apps/user-service/README.md):*** Service to manage users.

### Tools
The Tourney platform includes multiple tools to support development & documentation. These are stored in the `tools/` directory:
- ***[Coverage Merge](tools/coverage-merge/README.md):*** Tool to merge coverage tests from the microservices.
- ***[ES Lint](tools/eslint/README.md):*** Tool to manage ES linting.
- ***[Swagger Documentation](tools/swagger-doc/README.md):*** Tool to generate swagger api documentation.

### Docker Services
To aid in the local development of the Tourney platform, Docker is used to manage the various services to help run the Tourney Platform.
These files are stored in the `docker-services/` directory. More information can be found [here](docker-services/README.md).


## Installation
***Note:*** All commands should be run in the root directory of the repository.

1. Install [docker & docker compose](https://docs.docker.com/get-docker/)
2. Install [node](https://nodejs.org/en/blog/release/v16.16.0) version 16.x
3. Start the docker services by running:
    ```bash
   npm run start:local:docker-services
   ```
4. Install the project dependencies by running:. ***Note:*** this will also install git hooks via husky. [Learn more](#husky-git-hooks) below.
    ```bash
   npm install
   ```
5. Generate prisma schema by running:
    ```bash
   npm run prisma:generate:all
   ```
   
## Starting the apps
1. Start the docker services by running:
    ```bash
   npm run start:local:docker-services
   ```
2. Start the various microservices: 
   1. Logger Service:
       ```bash
       npm run start:local:logger-service
       ```
   2. Core Service:
       ```bash
       npm run start:local:core-service
       ```
   3. User Service:
       ```bash
       npm run start:local:user-service
       ```
   4. Gateway Service:
       ```bash
       npm run start:local:gateway-service
       ```

## Stopping the apps
1. Stop the processes for each microservice
2. Stop the docker services:
    ```bash
   npm run stop:local:docker-services
   ```

## Git Commit
The project adheres to the commit message [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)
The structure used is
```
<type>: <issue title> [<issue-code>]
Closes <issue code>
```

An example commit would be:
```
feat: add http client for identity provider [#256]
Closes #256
```

The supported `types` for this project are defined in `commitlint.config.js`.

This commit structure is enforced as part of the pre-commit hook configured via husky, as well as in the pipelines.

## Husky (git hooks)
Husky is used to register some actions during some git hooks. At the moment, the following git hooks are registered:
- commit-msg: Validate commit messages against `commitlint.config.js`
- pre-commit: Automatically format code

## Pipeline
Every merge request to the branch `dev` and `main` will trigger a detached pipeline.
The pipeline has the following jobs:

- Merge Request
   - install the npm dependencies.
   - Lint the commit message.
   - Lint the source or each microservice as well as run a code format check via prettier.
   - run pipeline jobs for each respective microservice **IF** their source code has been modified. Lean more in each microservice directory.
  
- Dev branch
  - install the npm dependencies.
  - Lint the commit message.
  - Lint the source or each microservice as well as run a code format check via prettier.
  - run pipeline jobs for each respective microservice.
  - Merge coverage test result from each microservice.
  - Deploy gitlab pages:
    - move coverage report to gitlab pages
    - move swagger documentation to gitlab pages
