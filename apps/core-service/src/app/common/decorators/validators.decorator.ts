import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export type relationOperators = '<' | '<=' | '>' | '>=' | '==' | '===' | '!=';
const relationStringMap = {
  '>': 'greater than',
  '>=': 'greater than or equal',
  '<': 'less than',
  '<=': 'less than or equal',
  '==': 'equal',
  '===': 'strict equal',
  '!=': 'not equal',
};

export function IsISO8601Relational(
  relation: relationOperators,
  property: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: object, propertyName: string) {
    registerDecorator({
      name: 'IsISO8601Relational',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];

          if (isNaN(Date.parse(value)) || isNaN(Date.parse(relatedValue))) {
            // TODO: find a way to return this error message back
            console.error('dateValue or relatedDateValue are not valid ISO8601 strings.');
            return false;
          }

          const dateValue = new Date(Date.parse(value));
          const relatedDateValue = new Date(Date.parse(relatedValue));

          switch (relation) {
            case '>':
              return dateValue > relatedDateValue;
            case '>=':
              return value >= relatedValue;
            case '<':
              return value < relatedValue;
            case '<=':
              return value <= relatedValue;
            case '==':
              return value == relatedValue;
            case '!=':
              return value != relatedValue;
            default:
              return false;
          }
        },
        defaultMessage(args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          return `${args.property} must be ${relationStringMap[relation]} ${relatedPropertyName}`;
        },
      },
    });
  };
}
