import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ResponseInterceptor<T> implements NestInterceptor<T, Record<string, unknown>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Record<string, unknown>> {
    return next.handle().pipe(
      map(data => ({
        message: data?.message || 'ACK',
        result: data?.result || {},
      })),
    );
  }
}

export const ResponseInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  useClass: ResponseInterceptor,
};
