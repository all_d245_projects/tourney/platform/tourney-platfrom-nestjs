import { GetCollectionDto } from '@tourney-platform/core-service/app/common/dto/get-collection.dto';
import { GamePlatform, GamePlatformGeneration, GameStatus } from '@tourney-platform/core-service/prisma/client';
import { IsArray, IsEnum, IsString } from 'class-validator';

export class GetGameDTO extends GetCollectionDto {}

export class CreateGameDTO {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsArray()
  @IsEnum(GamePlatform, { each: true })
  platform: GamePlatform[];

  @IsArray()
  @IsEnum(GamePlatformGeneration, { each: true })
  platformGeneration: GamePlatformGeneration[];

  @IsEnum(GameStatus)
  status: GameStatus;
}
