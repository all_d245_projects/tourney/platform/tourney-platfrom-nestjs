import { Injectable } from '@nestjs/common';
import { DatabaseFailureHandler } from '@tourney-platform/core-service/app/common/decorators/database-exception.decorator';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { CreateGameDTO } from '@tourney-platform/core-service/app/modules/games/dto/games.dto';
import { Game } from '@tourney-platform/core-service/prisma/client';

@DatabaseFailureHandler
@Injectable()
export class GamesService {
  constructor(private prismaService: PrismaService) {}

  async getGames(): Promise<Game[]> {
    return await this.prismaService.game.findMany();
  }

  async createGame(gameData: CreateGameDTO): Promise<Game> {
    return await this.prismaService.game.create({
      data: {
        ...gameData,
        createdDate: new Date(),
      },
    });
  }
}
