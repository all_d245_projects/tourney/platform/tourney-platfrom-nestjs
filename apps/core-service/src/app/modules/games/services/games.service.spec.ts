import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { CreateGameDTO } from '@tourney-platform/core-service/app/modules/games/dto/games.dto';
import { GamesService } from '@tourney-platform/core-service/app/modules/games/services/games.service';
import {
  GamePlatform,
  GamePlatformGeneration,
  GameStatus,
  PrismaClient,
} from '@tourney-platform/core-service/prisma/client';
import { GameTestHelper } from '@tourney-platform/core-service/test/helpers/game-test.helper';
import { plainToInstance } from 'class-transformer';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';

describe('GamesService', () => {
  let gamesService: GamesService;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GamesService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    gamesService = module.get(GamesService);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(gamesService).toBeDefined();
  });

  describe('getGames', () => {
    it('should return an array of games', async () => {
      const expectedResult = GameTestHelper.getMockGames();

      prisma.game.findMany.mockResolvedValueOnce(expectedResult);

      expect(gamesService.getGames()).resolves.toBe(expectedResult);
    });
  });

  describe('createGame', () => {
    it('should return a single game', async () => {
      const expectedResult = GameTestHelper.getMockGames(1)[0];
      const gameCreateDto = plainToInstance(CreateGameDTO, {
        name: 'FIFA 22',
        description: 'Just another FIFA game.',
        platform: [GamePlatform.PLAYSTATION],
        platformGeneration: [GamePlatformGeneration.PS5],
        status: GameStatus.ACTIVE,
      });

      prisma.game.create.mockResolvedValueOnce(expectedResult);

      expect(gamesService.createGame(gameCreateDto)).resolves.toBe(expectedResult);
    });
  });
});
