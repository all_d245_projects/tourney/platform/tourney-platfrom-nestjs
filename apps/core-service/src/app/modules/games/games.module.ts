import { Module } from '@nestjs/common';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { GamesController } from '@tourney-platform/core-service/app/modules/games/controllers/games.controller';
import { GamesService } from '@tourney-platform/core-service/app/modules/games/services/games.service';

@Module({
  providers: [GamesService, PrismaService],
  controllers: [GamesController],
})
export class GamesModule {}
