import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ControllerResponseDto } from '@tourney-platform/core-service/app/common/dto/response.dto';
import { CreateGameDTO } from '@tourney-platform/core-service/app/modules/games/dto/games.dto';
import { GamesService } from '@tourney-platform/core-service/app/modules/games/services/games.service';

export enum GamesResponseMessage {
  GET_GAMES_OK = 'get-games OK',
  CREATE_GAME_OK = 'create-game OK',
}

@Controller()
export class GamesController {
  constructor(private readonly gamesService: GamesService) {}

  @MessagePattern('get-games')
  async getGames(): Promise<ControllerResponseDto> {
    const games = await this.gamesService.getGames();

    return {
      message: GamesResponseMessage.GET_GAMES_OK,
      result: games,
    };
  }

  @MessagePattern('create-game')
  async createGame(@Payload() data: CreateGameDTO): Promise<ControllerResponseDto> {
    const game = await this.gamesService.createGame(data);

    return {
      message: GamesResponseMessage.CREATE_GAME_OK,
      result: game,
    };
  }
}
