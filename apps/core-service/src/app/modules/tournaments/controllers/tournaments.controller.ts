import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ControllerResponseDto } from '@tourney-platform/core-service/app/common/dto/response.dto';
import { CreateTournamentDTO } from '@tourney-platform/core-service/app/modules/tournaments/dto/tournaments.dto';
import { TournamentsService } from '@tourney-platform/core-service/app/modules/tournaments/services/tournaments.service';

export enum TournamentsResponseMessage {
  GET_TOURNAMENTS_OK = 'get-tournaments OK',
  CREATE_TOURNAMENT_OK = 'create-tournament OK',
}

@Controller()
export class TournamentsController {
  constructor(private readonly tournamentsService: TournamentsService) {}

  @MessagePattern('get-tournaments')
  async getTournaments(): Promise<ControllerResponseDto> {
    const tournaments = await this.tournamentsService.getTournaments();

    return {
      message: TournamentsResponseMessage.GET_TOURNAMENTS_OK,
      result: tournaments,
    };
  }

  @MessagePattern('create-tournament')
  async createGame(@Payload() data: CreateTournamentDTO): Promise<ControllerResponseDto> {
    const tournament = await this.tournamentsService.createTournament(data);

    return {
      message: TournamentsResponseMessage.CREATE_TOURNAMENT_OK,
      result: tournament,
    };
  }
}
