import { IsISO8601Relational } from '@tourney-platform/core-service/app/common/decorators/validators.decorator';
import { GetCollectionDto } from '@tourney-platform/core-service/app/common/dto/get-collection.dto';
import { GamePlatform, GamePlatformGeneration, TournamentStatus } from '@tourney-platform/core-service/prisma/client';
import { Type } from 'class-transformer';
import { IsArray, IsEnum, IsISO8601, IsNotEmpty, IsString, IsUUID, ValidateNested } from 'class-validator';

class TournamentGameDto {
  @IsUUID()
  gameId: string;
  @IsArray()
  @IsEnum(GamePlatform, { each: true })
  gamePlatform: GamePlatform[];

  @IsArray()
  @IsEnum(GamePlatformGeneration, { each: true })
  gamePlatformGeneration: GamePlatformGeneration[];
}

class ParticipantsDto {
  @IsUUID()
  playerId: string;
}

export class GetTournamenetsDTO extends GetCollectionDto {}

export class CreateTournamentDTO {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsISO8601({ strict: true, strictSeparator: true })
  startDate: string;

  @IsISO8601({ strict: true, strictSeparator: true })
  @IsISO8601Relational('<', 'startDate')
  invitationStartDate: string;

  @IsISO8601({ strict: true, strictSeparator: true })
  @IsISO8601Relational('<', 'startDate')
  @IsISO8601Relational('>', 'invitationStartDate')
  invitationEndDate: string;

  @ValidateNested()
  @Type(() => TournamentGameDto)
  tournamentGame: TournamentGameDto;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ParticipantsDto)
  participants: ParticipantsDto;

  @IsEnum(TournamentStatus)
  status: TournamentStatus;
}
