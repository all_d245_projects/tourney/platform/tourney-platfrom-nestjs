import { Injectable } from '@nestjs/common';
import { DatabaseFailureHandler } from '@tourney-platform/core-service/app/common/decorators/database-exception.decorator';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { CreateTournamentDTO } from '@tourney-platform/core-service/app/modules/tournaments/dto/tournaments.dto';
import { Tournament } from '@tourney-platform/core-service/prisma/client';

@DatabaseFailureHandler
@Injectable()
export class TournamentsService {
  constructor(private prismaService: PrismaService) {}

  async getTournaments(): Promise<Tournament[]> {
    return await this.prismaService.tournament.findMany();
  }

  async createTournament(tournamentData: CreateTournamentDTO): Promise<Tournament> {
    return await this.prismaService.tournament.create({
      data: {
        ...tournamentData,
        createdDate: new Date().toISOString(),
      },
    });
  }
}
