import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { CreateTournamentDTO } from '@tourney-platform/core-service/app/modules/tournaments/dto/tournaments.dto';
import { TournamentsService } from '@tourney-platform/core-service/app/modules/tournaments/services/tournaments.service';
import {
  GamePlatform,
  GamePlatformGeneration,
  PrismaClient,
  TournamentStatus,
} from '@tourney-platform/core-service/prisma/client';
import { TournamentTestHelper } from '@tourney-platform/core-service/test/helpers/tournament-test.helper';
import { plainToInstance } from 'class-transformer';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';

describe('TournamentsService', () => {
  let tournamentsService: TournamentsService;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TournamentsService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    tournamentsService = module.get(TournamentsService);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(tournamentsService).toBeDefined();
  });

  describe('getTournaments', () => {
    it('should return an array of tournaments', async () => {
      const expectedResult = TournamentTestHelper.getMockTournaments('GET');

      prisma.tournament.findMany.mockResolvedValueOnce(expectedResult);

      await expect(tournamentsService.getTournaments()).resolves.toBe(expectedResult);
    });
  });

  describe('createTournament', () => {
    it('should return a single tournament', async () => {
      const expectedResult = TournamentTestHelper.getMockTournaments('GET', 1)[0];
      const oneDayMilli = 24 * 60 * 60 * 1000;
      const tournamentCreateDto = plainToInstance(CreateTournamentDTO, {
        name: 'Tournament 1',
        description: 'Some description.',
        startDate: new Date(Date.now() + 7 * oneDayMilli),
        invitationStartDate: new Date(),
        invitationEndDate: new Date(Date.now() + 5 * oneDayMilli),
        tournamentGame: {
          gameId: 'some-game-uuid',
          gamePlatform: [GamePlatform.PLAYSTATION],
          gamePlatformGeneration: [GamePlatformGeneration.PS5],
        },
        participants: [
          {
            playerId: 'some-player-uuid',
          },
        ],
        status: TournamentStatus.CREATED,
        createdDate: new Date(),
      });

      prisma.tournament.create.mockResolvedValueOnce(expectedResult);

      await expect(tournamentsService.createTournament(tournamentCreateDto)).resolves.toBe(expectedResult);
    });
  });
});
