import { Module } from '@nestjs/common';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { TournamentsController } from '@tourney-platform/core-service/app/modules/tournaments/controllers/tournaments.controller';
import { TournamentsService } from '@tourney-platform/core-service/app/modules/tournaments/services/tournaments.service';

@Module({
  providers: [TournamentsService, PrismaService],
  controllers: [TournamentsController],
})
export class TournamentsModule {}
