import { Module } from '@nestjs/common';
import { ResponseInterceptorProvider } from '@tourney-platform/core-service/app/common/interceptors/response.interceptor';
import { GamesModule } from '@tourney-platform/core-service/app/modules/games/games.module';
import { LoggerModule } from '@tourney-platform/core-service/app/modules/logger/logger.module';
import { NatsClientModule } from '@tourney-platform/core-service/app/modules/nats/nats-client.module';
import { TournamentsModule } from '@tourney-platform/core-service/app/modules/tournaments/tournaments.module';
import ConfigModuleConfig from '@tourney-platform/core-service/config/modules/config-module.config';

@Module({
  imports: [ConfigModuleConfig, LoggerModule, NatsClientModule, GamesModule, TournamentsModule],
  providers: [ResponseInterceptorProvider],
})
export class AppModule {}
