import { registerAs } from '@nestjs/config';
import { RpcException } from '@nestjs/microservices';
import { Environment } from '@tourney-platform/core-service/app/common/validation/environmentVariable.validation';
import { ValidationError } from 'class-validator';

export default registerAs('app', () => ({
  environment: process.env.NODE_ENV,
  globalValidationPipeOptions: {
    exceptionFactory: (validationErrors: ValidationError[]) => {
      const result = {};

      validationErrors.forEach(error => {
        if (!result.hasOwnProperty(error.property)) {
          result[error.property] = [];
        }

        const message = error.constraints ? error.constraints[Object.keys(error.constraints)[0]] : error.constraints;

        result[error.property].push(message);
      });

      throw new RpcException({
        message: 'Validation Error',
        result,
      });
    },
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
    disableErrorMessages: process.env.NODE_ENV === Environment.Production,
    validationError: {
      value: true,
      target: true,
    },
  },
}));
