import { registerAs } from '@nestjs/config';
import { Transport } from '@nestjs/microservices';

export default registerAs('nats', () => {
  const transport = Transport.NATS;
  const host = process.env.NATS_HOST;
  const port = parseInt(process.env.NATS_PORT || '4222', 10);

  return {
    coreService: {
      transport,
      options: {
        name: 'core-service-nestjs',
        servers: `${host}:${port}`,
      },
    },
    loggerService: {
      transport,
      options: {
        name: 'logger-service-nestjs',
        servers: `${host}:${port}`,
      },
    },
  };
});
