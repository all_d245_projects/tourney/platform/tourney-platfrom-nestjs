import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from '@tourney-platform/core-service/app/app.module';
import { LoggerService } from '@tourney-platform/core-service/app/modules/logger/services/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  const configService = app.get(ConfigService);
  const coreServiceNatsConfig = configService.get('nats.coreService');

  app.useLogger(app.get(LoggerService));

  app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

  app.connectMicroservice<MicroserviceOptions>(coreServiceNatsConfig, {
    inheritAppConfig: true,
  });

  await app.startAllMicroservices();

  Logger.log(`NATS ready and listening on ${coreServiceNatsConfig.options.servers}`, 'AppBootstrap');
  Logger.log('CORE-SERVICE ready...', 'AppBootstrap');
}
bootstrap();
