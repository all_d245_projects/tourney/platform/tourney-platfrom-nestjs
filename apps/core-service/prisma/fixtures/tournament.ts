import {
  GamePlatform,
  GamePlatformGeneration,
  PrismaClient,
  TournamentStatus,
} from '@tourney-platform/core-service/prisma/client';

const oneDayMilli = 24 * 60 * 60 * 1000;
const tournaments = [
  {
    name: 'Tournament 1',
    description: 'Some description.',
    startDate: new Date(Date.now() + 7 * oneDayMilli).toISOString(),
    invitationStartDate: new Date().toISOString(),
    invitationEndDate: new Date(Date.now() + 5 * oneDayMilli).toISOString(),
    tournamentGame: {
      gameId: 'some-game-uuid',
      gamePlatform: [GamePlatform.PLAYSTATION],
      gamePlatformGeneration: [GamePlatformGeneration.PS5],
    },
    participants: [
      {
        playerId: 'some-player-uuid',
      },
    ],
    status: TournamentStatus.CREATED,
    createdDate: new Date().toISOString(),
  },
];

export default async (prisma: PrismaClient) => {
  try {
    await prisma.tournament.deleteMany();
    console.log('Deleted records in Tournament collection');

    await prisma.tournament.createMany({
      data: tournaments,
    });
  } catch (err) {
    console.error('Tournament seeding failed...');
    console.warn(err);
  }
};
