import { PrismaClient } from '@tourney-platform/core-service/prisma/client';
import { GamePlatform, GamePlatformGeneration, GameStatus } from '@tourney-platform/core-service/prisma/client';

const game = [
  {
    name: 'FIFA 22',
    description: 'Just another FIFA game.',
    platform: [GamePlatform.PLAYSTATION],
    platformGeneration: [GamePlatformGeneration.PS5],
    status: GameStatus.ACTIVE,
    createdDate: new Date(),
    modifiedDate: new Date(),
  },
];
export default async (prisma: PrismaClient) => {
  try {
    await prisma.game.deleteMany();
    console.log('Deleted records in Game collection');

    await prisma.game.createMany({
      data: game,
    });
  } catch (err) {
    console.error('Game seeding failed...');
    console.warn(err);
  }
};
