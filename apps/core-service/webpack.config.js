const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = function (options) {
  return {
    ...options,
    plugins: [
      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.resolve(__dirname, 'prisma/generated/schema.prisma'),
            to: path.resolve(__dirname, '../../dist/apps/core-service'),
          },
        ],
      }),
    ],
  };
};
