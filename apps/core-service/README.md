# Core Service - NestJS

## Description
Core service responsible for the mechanisms and logic of the tourney platform..

## Installation
1. Install dependencies
    ```bash
    npm install
    ```
2. Copy environment variable files
    ```bash
    cp .env.sample .env && cp sample.env .env.test
    ```
<br/>

## Configuration
The project is configured in two primary locations:

**.env file**
The `.env` file located at the root of this directory (after copying the `.env.example` file) and it stores configurations which can be modified per each environment.
The available configurations are defined below:

| Environment Variable | Default value                                                                                                                                                            | Description                                               |
|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| **NODE_ENV**         | `local`                                                                                                                                                                  | Node environment: valid options include [`local`, `test`] |
| **NATS_HOST**        | `localhost`                                                                                                                                                              | NATS host for connecting to the NATS service              |
| **NATS_PORT**        | `4222`                                                                                                                                                                   | NATS port for connecting to the NATS service              |
| **DATABASE_URL**     | `mongodb://core-service-dev:core-service-dev@localhost:27017,localhost:27018,localhost:27018/core-service-dev?replicaSet=tourney-platform-rs&authSource=admin&tls=false` | Connection url to MongoDB replica set                     |



**configuration files**
The configuration files are Nest.js configurations which modify certain aspects of the application.
These files are located in the direction `src/config/registered`.
The following files can be modified:
- `src/config/registered/app.config.ts`: This file includes various app-wide configurations such as environment, global validation behaviour, etc.
- `src/config/registered/nats.config.ts`: This file includes NATS related configurations.

## Prisma Database
The ORM for this microservice is [Prisma]().
It is required to generate the schema by running the command `npm run -w=@app/core-service prisma:generate`

## Running the app

```bash
# start application in watch mode using local environment
$ npm run start:local

# start application via a REPL environment (useful for testing services)
$ npm run start:repl
```

<br/>

## Test
```bash
# unit tests
npm run test:unit

# unit test coverage
npm run test:unit:cov

# unit test pipeline
npm run test:unit:ci

# e2e tests
npm run test:e2e

# e2e tests coverage
npm run test:e2e:cov

# e2e tests pipeline
npm run test:e2e:ci
```

## CI/CD Pipeline
The pipeline job for this microservice will be added to the merge request pipeline when files related to this microservice have been modified.
The jobs for this microservice include:
- build the gate-way service via `nest build`
- prepare the .env file
- execute unit tests
- execute e2e tests