import { TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { AppTestHelper } from '@tourney-platform/core-service/test/helpers/app-test.helper';
import { AssertionTestHelper } from '@tourney-platform/core-service/test/helpers/assertion-test.helper';
import { TournamentTestHelper } from '@tourney-platform/core-service/test/helpers/tournament-test.helper';
import { Observable } from 'rxjs';

describe('TournamentsController (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let prisma: PrismaService;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule = await AppTestHelper.createE2eTestingModule();

    await appTestHelper.init(testingModule);

    prisma = appTestHelper.getApp().get(PrismaService);
    await prisma.tournament.deleteMany();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('get-tournaments (Handler)', () => {
    it('returns tournaments response', done => {
      TournamentTestHelper.createTournaments(prisma, 'GET')
        .then(() => {
          const response: Observable<any> = appTestHelper.getNatsClient().send('get-tournaments', {});

          response.subscribe(response => {
            AssertionTestHelper.isValidResponse(response, 'get-tournaments OK');

            expect(response.result).toHaveLength(5);

            response.result.forEach(tournament => {
              TournamentTestHelper.assertIsValidTournamentModel(tournament);
            });

            done();
          });
        })
        .catch(err => {
          throw err;
        });
    });
  });

  describe('create-tournament (Handler)', () => {
    it('returns tournament after creating it', done => {
      const tournamentCreateData = TournamentTestHelper.getMockTournaments('CREATE', 1)[0];

      const response: Observable<any> = appTestHelper.getNatsClient().send('create-tournament', tournamentCreateData);

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'create-tournament OK');
        TournamentTestHelper.assertIsValidTournamentModel(response.result);

        done();
      });
    });
  });
});
