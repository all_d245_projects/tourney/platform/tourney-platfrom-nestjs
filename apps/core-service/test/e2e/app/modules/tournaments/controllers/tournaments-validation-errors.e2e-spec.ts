import { TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { AppTestHelper } from '@tourney-platform/core-service/test/helpers/app-test.helper';
import { AssertionTestHelper } from '@tourney-platform/core-service/test/helpers/assertion-test.helper';
import { catchError, Observable, of } from 'rxjs';

describe('TournamentsController Validation Errors (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let prisma: PrismaService;
  const oneDayMilli = 24 * 60 * 60 * 1000;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule = await AppTestHelper.createE2eTestingModule();

    await appTestHelper.init(testingModule);

    prisma = appTestHelper.getApp().get(PrismaService);
    await prisma.tournament.deleteMany();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('create-tournament (Handler)', () => {
    it('returns validation errors when no payload is sent creating a tournament', done => {
      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-tournament', {})
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'Validation Error');
        expect(Object.keys(response.result)).toHaveLength(7);

        done();
      });
    });

    it('returns validation errors when invitationStartDate is greater than start Date', done => {
      const payload = {
        startDate: new Date(Date.now() + 5 * oneDayMilli).toISOString(),
        invitationStartDate: new Date(Date.now() + 7 * oneDayMilli).toISOString(),
      };

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-tournament', payload)
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'Validation Error');
        expect(response.result.invitationStartDate).toContain('invitationStartDate must be less than startDate');
        done();
      });
    });

    it('returns validation errors when invitationEndDate is greater than invitationStartDate Date', done => {
      const payload = {
        startDate: new Date(Date.now() + 15 * oneDayMilli).toISOString(),
        invitationStartDate: new Date(Date.now() + 10 * oneDayMilli).toISOString(),
        invitationEndDate: new Date(Date.now() + 5 * oneDayMilli).toISOString(),
      };

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-tournament', payload)
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'Validation Error');
        expect(response.result.invitationEndDate).toContain(
          'invitationEndDate must be greater than invitationStartDate',
        );
        done();
      });
    });

    it('returns validation errors when invitationEndDate is greater than startDate Date', done => {
      const payload = {
        startDate: new Date(Date.now() + 7 * oneDayMilli).toISOString(),
        invitationEndDate: new Date(Date.now() + 10 * oneDayMilli).toISOString(),
      };

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-tournament', payload)
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'Validation Error');
        expect(response.result.invitationEndDate).toContain('invitationEndDate must be less than startDate');
        done();
      });
    });
  });
});
