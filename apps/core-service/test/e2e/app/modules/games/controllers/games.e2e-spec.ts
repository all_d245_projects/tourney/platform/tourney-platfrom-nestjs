import { TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import { GamePlatform, GamePlatformGeneration, GameStatus } from '@tourney-platform/core-service/prisma/client';
import { AppTestHelper } from '@tourney-platform/core-service/test/helpers/app-test.helper';
import { AssertionTestHelper } from '@tourney-platform/core-service/test/helpers/assertion-test.helper';
import { GameTestHelper } from '@tourney-platform/core-service/test/helpers/game-test.helper';
import { catchError, Observable, of } from 'rxjs';

describe('GamesController (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let prisma: PrismaService;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule = await AppTestHelper.createE2eTestingModule();

    await appTestHelper.init(testingModule);

    prisma = appTestHelper.getApp().get(PrismaService);
    await prisma.game.deleteMany();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('get-games (Handler)', () => {
    it('returns games response', done => {
      prisma.game
        .createMany({
          data: GameTestHelper.getMockGames().map(obj => ({
            ...obj,
            createdDate: new Date(),
          })),
        })
        .then(() => {
          const response: Observable<any> = appTestHelper.getNatsClient().send('get-games', {});

          response.subscribe(response => {
            expect(response.message).toEqual('get-games OK');
            expect(response).toHaveProperty('result');
            expect(response.result).toHaveLength(5);

            response.result.forEach(game => {
              expect(game).toHaveProperty('id');
              expect(game).toHaveProperty('name');
              expect(game).toHaveProperty('description');
              expect(game).toHaveProperty('platform');
              expect(game).toHaveProperty('platformGeneration');
              expect(game).toHaveProperty('status');
              expect(game).toHaveProperty('createdDate');
            });

            done();
          });
        })
        .catch(err => {
          throw err;
        });
    });
  });

  describe('create-game (Handler)', () => {
    it('returns game after creating it', done => {
      const gameCreateData = {
        name: 'FIFA 22',
        description: 'Just another FIFA game.',
        platform: [GamePlatform.PLAYSTATION],
        platformGeneration: [GamePlatformGeneration.PS5],
        status: GameStatus.ACTIVE,
      };

      const response: Observable<any> = appTestHelper.getNatsClient().send('create-game', gameCreateData);

      response.subscribe(response => {
        expect(response.message).toEqual('create-game OK');
        expect(response).toHaveProperty('result');
        expect(response.result).toHaveProperty('id');
        expect(response.result.name).toEqual(gameCreateData.name);

        done();
      });
    });

    it('returns validation errors when no payload is sent creating a game', done => {
      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('create-game', {})
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        AssertionTestHelper.isValidResponse(response, 'Validation Error');
        expect(Object.keys(response.result)).toHaveLength(5);

        done();
      });
    });
  });
});
