import { faker } from '@faker-js/faker';
import { PrismaService } from '@tourney-platform/core-service/app/common/services/prisma/prisma.service';
import {
  GamePlatform,
  GamePlatformGeneration,
  Player,
  Tournament,
  TournamentStatus,
} from '@tourney-platform/core-service/prisma/client';
import { AssertionTestHelper } from '@tourney-platform/core-service/test/helpers/assertion-test.helper';
import { UniqueEnforcer } from 'enforce-unique';

type Mode = 'GET' | 'CREATE';

export class TournamentTestHelper {
  static getMockTournaments(mode: Mode, numberOfTournaments = 5): Tournament[] {
    const tournaments: Tournament[] = [];
    const uniqueTournamentNameEnforcer: UniqueEnforcer = new UniqueEnforcer();
    const uniqueUuidEnforcer: UniqueEnforcer = new UniqueEnforcer();

    for (let i = 0; i < numberOfTournaments; i++) {
      const tournament = this.getRawTournamentData(uniqueTournamentNameEnforcer, uniqueUuidEnforcer, mode);

      tournaments.push(tournament as Tournament);
    }

    return tournaments;
  }

  static createTournaments(prisma: PrismaService, mode: Mode, numberOfTournaments = 5): Promise<any> {
    return prisma.tournament.createMany({
      data: TournamentTestHelper.getMockTournaments(mode, numberOfTournaments).map(obj => ({
        ...obj,
        createdDate: new Date(),
      })),
    });
  }

  static assertIsValidTournamentModel(tournament: object) {
    const expectedTournamentKeys = [
      'id',
      'name',
      'description',
      'startDate',
      'invitationStartDate',
      'invitationEndDate',
      'tournamentGame',
      'participants',
      'status',
      'createdDate',
      'modifiedDate',
    ];
    const expectedTournamentGameKeys = ['gameId', 'gamePlatform', 'gamePlatformGeneration'];
    const expectedParticipantsKeys = ['playerId'];

    expect(AssertionTestHelper.isValidModelStructure(tournament, expectedTournamentKeys)).toBeTruthy();
    expect(
      AssertionTestHelper.isValidModelStructure(tournament['tournamentGame'], expectedTournamentGameKeys),
    ).toBeTruthy();

    tournament['participants'].forEach(participant => {
      expect(AssertionTestHelper.isValidModelStructure(participant, expectedParticipantsKeys)).toBeTruthy();
    });
  }

  private static getRawTournamentPlayerData(uniqueUuidEnforcer: UniqueEnforcer, numberOfPlayers = 5) {
    const players: Player[] = [];

    for (let i = 0; i < numberOfPlayers; i++) {
      players.push({
        playerId: uniqueUuidEnforcer.enforce(() => faker.string.uuid()),
      } as Player);
    }

    return players;
  }

  private static getRawTournamentData(
    uniqueTournamentNameEnforcer: UniqueEnforcer,
    uniqueUuidEnforcer: UniqueEnforcer,
    mode: Mode,
  ): Record<string, unknown> {
    const oneDayMilli = 24 * 60 * 60 * 1000;
    const id = faker.string.uuid();
    const name = uniqueTournamentNameEnforcer.enforce(() => faker.lorem.words({ min: 1, max: 3 }));
    const description = faker.lorem.sentences(3);
    const startDate = new Date(Date.now() + 7 * oneDayMilli).toISOString();
    const invitationStartDate = new Date(Date.now() + 3 * oneDayMilli).toISOString();
    const invitationEndDate = new Date(Date.now() + 5 * oneDayMilli).toISOString();
    const tournamentGame = {
      gameId: uniqueUuidEnforcer.enforce(() => faker.string.uuid()),
      gamePlatform: faker.helpers.arrayElements([GamePlatform.PLAYSTATION], {
        min: 1,
        max: 2,
      }),
      gamePlatformGeneration: faker.helpers.arrayElements([GamePlatformGeneration.PS4, GamePlatformGeneration.PS5], {
        min: 1,
        max: 2,
      }),
    };
    const participants = TournamentTestHelper.getRawTournamentPlayerData(uniqueUuidEnforcer);
    const status = faker.helpers.enumValue(TournamentStatus);
    const createdDate = faker.date.recent().toISOString();
    const modifiedDate = faker.date.future({ refDate: createdDate }).toISOString();

    const tournament = {
      name,
      description,
      startDate,
      invitationStartDate,
      invitationEndDate,
      tournamentGame,
      participants,
      status,
    };

    switch (mode) {
      case 'GET':
        return { id, createdDate, modifiedDate, ...tournament };
      case 'CREATE':
        return tournament;
    }
  }
}
