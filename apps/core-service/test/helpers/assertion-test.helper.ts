import { ControllerResponseDto } from '@tourney-platform/core-service/app/common/dto/response.dto';

export class AssertionTestHelper {
  static isValidResponse(res: any, message: string) {
    expect(res).toMatchObject(new ControllerResponseDto());
    expect(res.message).toEqual(message);
  }

  static isValidModelStructure(model: object, expectedKeys: string[]) {
    const keys = Object.keys(model);
    const expectedSet = new Set(expectedKeys);
    const modelSet = new Set(keys);

    return expectedSet.size === modelSet.size && [...expectedSet].every(element => modelSet.has(element));
  }
}
