const config = require('@tools/eslint.config');

module.exports = {
  ...config,
  ignorePatterns: ['.eslintrc.js'],
  parserOptions: {
    project: 'tsconfig.app.json',
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
};
