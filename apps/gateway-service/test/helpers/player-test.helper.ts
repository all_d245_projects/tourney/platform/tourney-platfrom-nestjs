import { faker } from '@faker-js/faker';
import { IdentityProvider } from '@tourney-platform/gateway-service/app/common/entities/player.entity';

export class PlayerTestHelper {
  static getMockPlayers(numberOfPlayers = 5): Record<string, unknown>[] {
    const players: Record<string, unknown>[] = [];

    for (let i = 0; i < numberOfPlayers; i++) {
      const player = this.getRawPlayerData();

      players.push(player);
    }

    return players;
  }

  private static getRawPlayerData(): Record<string, unknown> {
    const sex = faker.person.sexType();
    const firstName = faker.person.firstName(sex);
    const lastName = faker.person.lastName();
    const username = faker.internet.userName({ firstName, lastName });
    const email = faker.internet.email({ firstName, lastName });

    const player = {
      id: faker.string.uuid(),
      playerProfile: {
        email,
        username,
        firstName,
        lastName,
      },
      playerIdentityProvider: {
        provider: faker.helpers.enumValue(IdentityProvider),
        providerId: faker.string.alphanumeric(8),
        providerData: {
          first: 'data',
          second: 'data',
          third: 'data',
        },
      },
    };

    return player;
  }
}
