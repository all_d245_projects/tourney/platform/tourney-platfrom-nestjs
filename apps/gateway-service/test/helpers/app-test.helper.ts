import { ExecutionContext, HttpServer, INestApplication, ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthGuard } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@tourney-platform/gateway-service/app/app.module';

export class AppTestHelper {
  private app: INestApplication;
  private server: HttpServer;
  private scopes: string;

  constructor(config: Record<string, any> = {}) {
    if ('scopes' in config) {
      this.scopes = config.scopes.join(' ');
    }
  }

  public init(testingModule: TestingModule) {
    this.app = this.createApplication(testingModule);
    this.server = this.app.getHttpServer();
  }

  public getApp(): INestApplication {
    return this.app;
  }

  public getServer(): HttpServer {
    return this.server;
  }

  public getScopes(): string {
    return this.scopes;
  }

  public async terminate() {
    this.server.close();
    await this.app.close();
  }

  static async createTestingModule(): Promise<TestingModule> {
    const testingModule: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [],
    }).compile();

    return testingModule;
  }

  static async createTestingModuleWithAuthGuardMocked(scopes: string): Promise<TestingModule> {
    const testingModule: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers: [],
    })
      .overrideGuard(AuthGuard('jwt'))
      .useValue({
        canActivate: (context: ExecutionContext) => {
          const req = context.switchToHttp().getRequest();
          req.user = {
            scope: scopes,
          };
          return true;
        },
      })
      .compile();

    return testingModule;
  }

  private createApplication(testingModule: TestingModule): INestApplication {
    const configService: ConfigService = testingModule.get<ConfigService>(ConfigService);

    const app: INestApplication = testingModule.createNestApplication();

    app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

    app.enableVersioning({
      type: VersioningType.URI,
      defaultVersion: '1',
    });

    return app;
  }
}
