import { ApiResponseEntity } from '@tourney-platform/gateway-service/app/common/entities/api-response.entity';
import { PlayerEntity } from '@tourney-platform/gateway-service/app/common/entities/player.entity';
import { plainToInstance } from 'class-transformer';

export class AssertionTestHelper {
  static isValidApiResponse(res: any) {
    expect(res).toMatchObject(new ApiResponseEntity());
  }

  static isValidUnauthorizedResponse(res: any) {
    expect(res.status).toEqual(401);
    expect(res.body).toStrictEqual({
      statusCode: 401,
      message: 'Unauthorized',
    });
  }

  static resultIsValidPlayerEntity(res: any) {
    let result = res.body.result;
    result = plainToInstance(PlayerEntity, result);

    if (Array.isArray(result)) {
      result.forEach(elem => {
        expect(elem).toBeInstanceOf(PlayerEntity);
      });
    } else {
      expect(result).toBeInstanceOf(PlayerEntity);
    }
  }
}
