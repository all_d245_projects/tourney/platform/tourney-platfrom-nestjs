import { TestingModule } from '@nestjs/testing';
import { PlayersResponseMessage } from '@tourney-platform/gateway-service/app/modules/user-service/controllers/api/v1/players/players.controller';
import { PlayersService } from '@tourney-platform/gateway-service/app/modules/user-service/services/players/players.service';
import { AppTestHelper } from '@tourney-platform/gateway-service/test/helpers/app-test.helper';
import { AssertionTestHelper } from '@tourney-platform/gateway-service/test/helpers/assertion-test.helper';
import { PlayerTestHelper } from '@tourney-platform/gateway-service/test/helpers/player-test.helper';
import { of } from 'rxjs';
import request from 'supertest';

describe('PlayersController (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let playersService: PlayersService;
  const config: Record<string, unknown> = {};

  beforeEach(async () => {
    config.scopes = ['players:read', 'players:create'];

    appTestHelper = new AppTestHelper(config);

    const testingModule: TestingModule = await AppTestHelper.createTestingModuleWithAuthGuardMocked(
      appTestHelper.getScopes(),
    );

    appTestHelper.init(testingModule);

    await appTestHelper.getApp().init();

    playersService = appTestHelper.getApp().get(PlayersService);
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('/v1/players (GET)', () => {
    it('returns players response', async () => {
      const playersSeed = PlayerTestHelper.getMockPlayers(5);
      const expectedObservable = of({
        message: 'get-players OK',
        result: playersSeed,
      });

      jest.spyOn(playersService, 'getPlayers').mockReturnValue(expectedObservable);

      await request(appTestHelper.getApp().getHttpServer())
        .get('/v1/players')
        .expect(AssertionTestHelper.isValidApiResponse)
        .expect(AssertionTestHelper.resultIsValidPlayerEntity)
        .expect(200, {
          statusCode: 200,
          message: PlayersResponseMessage.GET_PLAYERS_OK,
          result: playersSeed,
        });
    });
  });

  describe('/v1/players/register (POST)', () => {
    it('returns created player response', async () => {
      const playerSeed = PlayerTestHelper.getMockPlayers(1)[0];
      const expectedObservable = of({
        message: 'register-player OK',
        result: playerSeed,
      });

      jest.spyOn(playersService, 'registerPlayer').mockReturnValue(expectedObservable);

      await request(appTestHelper.getApp().getHttpServer())
        .post('/v1/players/register')
        .send({
          playerProfile: {
            email: 'some-email4@domain.com',
            username: 'username',
          },
        })
        .expect(AssertionTestHelper.isValidApiResponse)
        .expect(AssertionTestHelper.resultIsValidPlayerEntity)
        .expect(201, {
          statusCode: 201,
          message: PlayersResponseMessage.REGISTER_PLAYER_OK,
          result: playerSeed,
        });
    });

    it('throws a 400 on invalid register player post body', async () => {
      const playerBody = {};
      await request(appTestHelper.getApp().getHttpServer())
        .post('/v1/players/register')
        .send(playerBody)
        .expect(AssertionTestHelper.isValidApiResponse)
        .expect(400);
    });
  });
});
