import { TestingModule } from '@nestjs/testing';
import { AppTestHelper } from '@tourney-platform/gateway-service/test/helpers/app-test.helper';
import { AssertionTestHelper } from '@tourney-platform/gateway-service/test/helpers/assertion-test.helper';
import request from 'supertest';

describe('PlayersController No-Auth (e2e)', () => {
  let appTestHelper: AppTestHelper;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule = await AppTestHelper.createTestingModule();

    appTestHelper.init(testingModule);

    await appTestHelper.getApp().init();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('/v1/players (GET)', () => {
    it('throws 401', async () => {
      await request(appTestHelper.getApp().getHttpServer())
        .get('/v1/players')
        .expect(AssertionTestHelper.isValidApiResponse)
        .expect(AssertionTestHelper.isValidUnauthorizedResponse);
    });
  });

  describe('/v1/players/register (POST)', () => {
    it('throws 401', async () => {
      await request(appTestHelper.getApp().getHttpServer())
        .post('/v1/players/register')
        .expect(AssertionTestHelper.isValidApiResponse)
        .expect(AssertionTestHelper.isValidUnauthorizedResponse);
    });
  });
});
