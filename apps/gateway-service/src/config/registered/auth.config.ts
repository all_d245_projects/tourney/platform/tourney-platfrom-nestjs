import { registerAs } from '@nestjs/config';

export default registerAs('auth', () => ({
  auth0: {
    issuerUrl: process.env.AUTH0_ISSUER_URL,
    audience: process.env.AUTH0_AUDIENCE,
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
  },
}));
