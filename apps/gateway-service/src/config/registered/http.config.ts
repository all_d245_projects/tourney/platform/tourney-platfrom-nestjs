import { registerAs } from '@nestjs/config';

export default registerAs('http', () => ({
  host: process.env.HTTP_HOST,
  port: parseInt(process.env.HTTP_PORT || '3009', 10),
}));
