import { registerAs } from '@nestjs/config';
import { DocumentBuilder } from '@nestjs/swagger';
import * as packageDotJson from '@tourney-platform/gateway-service/../package.json';
import { Environment } from '@tourney-platform/gateway-service/app/common/validation/environmentVariable.validation';

export default registerAs('app', () => ({
  environment: process.env.NODE_ENV,
  globalValidationPipeOptions: {
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
    disableErrorMessages: process.env.NODE_ENV === Environment.Production,
    validationError: {
      value: true,
      target: true,
    },
  },
  swagger: {
    builder: new DocumentBuilder()
      .setTitle(packageDotJson.name)
      .setDescription(packageDotJson.description)
      .setVersion(packageDotJson.version)
      .addBearerAuth({
        type: 'http',
        bearerFormat: 'JWT',
        in: 'Authorization',
      })
      .build(),
    options: {
      operationIdFactory: (methodKey: string) => methodKey,
    },
  },
}));
