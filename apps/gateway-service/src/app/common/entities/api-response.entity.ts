import { ApiProperty } from '@nestjs/swagger';

export class ApiResponseEntity {
  @ApiProperty()
  message: string;

  @ApiProperty()
  statusCode: number;

  @ApiProperty({ required: false })
  errors?: [] | undefined;

  @ApiProperty({ required: false })
  result?: any;
}
