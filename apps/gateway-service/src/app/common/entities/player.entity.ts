import { ApiProperty } from '@nestjs/swagger';

class PlayerProfile {
  @ApiProperty()
  email: string;

  @ApiProperty()
  username: string;

  @ApiProperty({ required: false })
  firstName?: string;

  @ApiProperty({ required: false })
  firstLast?: string;
}

class PlayerIdentityProfile {
  @ApiProperty()
  provider: IdentityProvider;

  @ApiProperty()
  providerId: string;

  @ApiProperty({ required: false })
  providerData?: Record<string, unknown>;
}

export enum IdentityProvider {
  AUTH0,
}

export class PlayerEntity {
  @ApiProperty()
  id: string;

  @ApiProperty()
  playerProfile: PlayerProfile;

  @ApiProperty({ required: false })
  playerIdentityProfile: PlayerIdentityProfile;
}
