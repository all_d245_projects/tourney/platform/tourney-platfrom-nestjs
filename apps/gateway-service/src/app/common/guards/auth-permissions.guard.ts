import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const routePermissions = this.reflector.get<string[]>('permissions', context.getHandler());

    if (!routePermissions) {
      return true;
    }

    const scopes = this.getScopes(context.getArgs()[0].user);

    const hasPermission = () => routePermissions.every(routePermission => scopes.includes(routePermission));

    return hasPermission();
  }

  private getScopes(claims: Record<string, unknown>): string[] {
    const scopes: unknown | string = claims?.scope;

    return typeof scopes === 'string' ? scopes.split(' ') : [];
  }
}
