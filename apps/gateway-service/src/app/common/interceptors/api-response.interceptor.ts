import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ApiResponseEntity } from '@tourney-platform/gateway-service/app/common/entities/api-response.entity';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiResponseInterceptor<T> implements NestInterceptor<T, ApiResponseEntity> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<ApiResponseEntity> {
    const response = context.switchToHttp().getResponse();

    return next.handle().pipe(
      map(data => ({
        message: data?.message || '',
        statusCode: response.statusCode,
        result: data?.result || {},
      })),
    );
  }
}

export const ResponseInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  useClass: ApiResponseInterceptor,
};
