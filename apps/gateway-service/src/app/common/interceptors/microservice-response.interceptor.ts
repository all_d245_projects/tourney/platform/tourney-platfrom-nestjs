import { CallHandler, ExecutionContext, HttpException, Injectable, NestInterceptor } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { MicroserviceResposne } from '@tourney-platform/gateway-service/app/common/types/microservice.type';
import { catchError, map, Observable } from 'rxjs';

@Injectable()
export class MicroserviceResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((response: MicroserviceResposne) => response.result),
      catchError(error => {
        switch (true) {
          case error instanceof RpcException:
          case error instanceof HttpException:
            throw error;

          default:
            throw 'message' in error ? new RpcException(error.message) : new RpcException(error.getMessage());
        }
      }),
    );
  }
}
