import { ClientProxy } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { NatsClientModule } from '@tourney-platform/gateway-service/app/modules/nats/nats-client.module';
import { CreatePlayerDto } from '@tourney-platform/gateway-service/app/modules/user-service/dto/players/players.dto';
import { PlayersService } from '@tourney-platform/gateway-service/app/modules/user-service/services/players/players.service';
import configModuleConfig from '@tourney-platform/gateway-service/config/modules/config-module.config';
import { PlayerTestHelper } from '@tourney-platform/gateway-service/test/helpers/player-test.helper';
import { plainToInstance } from 'class-transformer';
import { Observable, of } from 'rxjs';

describe('PlayersService', () => {
  let playersService: PlayersService;
  let natsService: ClientProxy;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [configModuleConfig, NatsClientModule],
      providers: [PlayersService],
    }).compile();

    playersService = module.get(PlayersService);
    natsService = module.get('NATS_USER_SERVICE_CLIENT');
  });

  it('should be defined', () => {
    expect(playersService).toBeDefined();
  });

  describe('getPlayers', () => {
    it('should return an observable of value array', done => {
      const playersSeed = PlayerTestHelper.getMockPlayers(5);
      const expectedObservable = of({
        message: 'get-players OK',
        result: playersSeed,
      });

      jest.spyOn(natsService, 'send').mockImplementation(() => expectedObservable);

      const getPlayersObservable = playersService.getPlayers();

      expect(getPlayersObservable).toBeInstanceOf(Observable);

      getPlayersObservable.subscribe(players => {
        expect(players.result).toHaveLength(5);
        expect(players.result[0].id).toEqual(playersSeed[0].id);
        done();
      });
    });
  });

  describe('registerPlayers', () => {
    it('should return an observable of value object', done => {
      const playerSeed = PlayerTestHelper.getMockPlayers(1)[0];
      const expectedObservable = of({
        message: 'register-player OK',
        result: playerSeed,
      });
      const playerData: CreatePlayerDto = plainToInstance(CreatePlayerDto, {});

      jest.spyOn(natsService, 'send').mockImplementation(() => expectedObservable);

      const getPlayerObservable = playersService.registerPlayer(playerData);

      expect(getPlayerObservable).toBeInstanceOf(Observable);

      getPlayerObservable.subscribe(player => {
        expect(player.result.id).toEqual(playerSeed.id);
        done();
      });
    });
  });
});
