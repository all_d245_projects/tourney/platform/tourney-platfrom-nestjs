import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { MicroserviceResposne } from '@tourney-platform/gateway-service/app/common/types/microservice.type';
import { CreatePlayerDto } from '@tourney-platform/gateway-service/app/modules/user-service/dto/players/players.dto';
import { Observable } from 'rxjs';

@Injectable()
export class PlayersService {
  constructor(@Inject('NATS_USER_SERVICE_CLIENT') private natsService: ClientProxy) {}

  getPlayers(): Observable<MicroserviceResposne> {
    return this.natsService.send('get-players', {});
  }

  registerPlayer(playerData: CreatePlayerDto): Observable<MicroserviceResposne> {
    return this.natsService.send('register-player', playerData);
  }
}
