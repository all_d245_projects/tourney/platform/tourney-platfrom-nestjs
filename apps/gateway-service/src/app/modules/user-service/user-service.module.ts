import { Module } from '@nestjs/common';
import { PlayersController } from '@tourney-platform/gateway-service/app/modules/user-service/controllers/api/v1/players/players.controller';
import { PlayersService } from '@tourney-platform/gateway-service/app/modules/user-service/services/players/players.service';

@Module({
  imports: [],
  providers: [PlayersService],
  controllers: [PlayersController],
})
export class UserServiceModule {}
