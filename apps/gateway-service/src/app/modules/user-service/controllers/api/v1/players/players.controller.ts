import { Body, Controller, Get, Post, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Permissions } from '@tourney-platform/gateway-service/app/common/decorators/auth-permissions.decorator';
import { PlayerEntity } from '@tourney-platform/gateway-service/app/common/entities/player.entity';
import { PermissionsGuard } from '@tourney-platform/gateway-service/app/common/guards/auth-permissions.guard';
import { MicroserviceResponseInterceptor } from '@tourney-platform/gateway-service/app/common/interceptors/microservice-response.interceptor';
import { MicroserviceResposne } from '@tourney-platform/gateway-service/app/common/types/microservice.type';
import { CreatePlayerDto } from '@tourney-platform/gateway-service/app/modules/user-service/dto/players/players.dto';
import { PlayersService } from '@tourney-platform/gateway-service/app/modules/user-service/services/players/players.service';
import { map, Observable } from 'rxjs';

export enum PlayersResponseMessage {
  GET_PLAYERS_OK = 'get-players OK',
  REGISTER_PLAYER_OK = 'register-player OK',
}

@ApiBearerAuth()
@ApiTags('user-service', 'players')
@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @Get()
  @UseInterceptors(MicroserviceResponseInterceptor)
  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Permissions('players:read')
  @ApiOkResponse({
    description: PlayersResponseMessage.GET_PLAYERS_OK,
    isArray: true,
    type: PlayerEntity,
  })
  getPlayers(): Observable<MicroserviceResposne> {
    return this.playersService.getPlayers().pipe(
      map(players => ({
        message: PlayersResponseMessage.GET_PLAYERS_OK,
        result: players,
      })),
    );
  }

  @Post('register')
  @UseInterceptors(MicroserviceResponseInterceptor)
  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Permissions('players:create')
  @ApiCreatedResponse({
    description: PlayersResponseMessage.REGISTER_PLAYER_OK,
    isArray: false,
    type: PlayerEntity,
  })
  registerPlayer(@Body() playerData: CreatePlayerDto): Observable<MicroserviceResposne> {
    return this.playersService.registerPlayer(playerData).pipe(
      map(player => ({
        message: PlayersResponseMessage.REGISTER_PLAYER_OK,
        result: player,
      })),
    );
  }
}
