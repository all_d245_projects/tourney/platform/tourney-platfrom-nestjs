import { GetCollectionDto } from '@tourney-platform/gateway-service/app/common/dto/request/get-collection.dto';
import { IdentityProvider } from '@tourney-platform/gateway-service/app/common/entities/player.entity';
import { Type } from 'class-transformer';
import { IsDefined, IsEmail, IsEnum, IsObject, IsOptional, IsString, ValidateNested } from 'class-validator';

export class GetPlayersDTO extends GetCollectionDto {}

class PlayerProfileDto {
  @IsEmail()
  email: string;

  @IsString()
  username: string;

  @IsOptional()
  @IsString()
  firstName?: string;

  @IsOptional()
  @IsString()
  lastName?: string;
}

class PlayerIdentityProviderDto {
  @IsEnum(IdentityProvider)
  provider: IdentityProvider;

  @IsString()
  providerId: string;

  @IsObject()
  @IsOptional()
  providerData?: Record<string, unknown> | null;
}

export class CreatePlayerDto {
  @ValidateNested()
  @Type(() => PlayerProfileDto)
  @IsDefined()
  playerProfile: PlayerProfileDto;

  @ValidateNested()
  @Type(() => PlayerIdentityProviderDto)
  @IsOptional()
  playerIdentityProvider?: PlayerIdentityProviderDto;
}
