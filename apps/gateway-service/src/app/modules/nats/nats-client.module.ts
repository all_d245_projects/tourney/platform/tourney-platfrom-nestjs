import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxyFactory } from '@nestjs/microservices';

const UserServiceNatsClient = {
  provide: 'NATS_USER_SERVICE_CLIENT',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const natsModuleOptions = configService.get('nats.userService');
    return ClientProxyFactory.create(natsModuleOptions);
  },
};

const LoggerServiceNatsClient = {
  provide: 'NATS_LOGGER_SERVICE_CLIENT',
  inject: [ConfigService],
  useFactory: (configService: ConfigService) => {
    const natsModuleOptions = configService.get('nats.loggerService');
    return ClientProxyFactory.create(natsModuleOptions);
  },
};

@Global()
@Module({
  providers: [LoggerServiceNatsClient, UserServiceNatsClient],
  exports: [LoggerServiceNatsClient, UserServiceNatsClient],
})
export class NatsClientModule {}
