import { Module } from '@nestjs/common';
import { ResponseInterceptorProvider } from '@tourney-platform/gateway-service/app/common/interceptors/api-response.interceptor';
import { NatsClientModule } from '@tourney-platform/gateway-service/app/modules/nats/nats-client.module';
import { UserServiceModule } from '@tourney-platform/gateway-service/app/modules/user-service/user-service.module';
import { Auth0Module } from '@tourney-platform/gateway-service/app/security/auth0/auth0.module';
import ConfigModuleConfig from '@tourney-platform/gateway-service/config/modules/config-module.config';

import { LoggerModule } from './modules/logger/logger.module';

@Module({
  imports: [ConfigModuleConfig, UserServiceModule, NatsClientModule, Auth0Module, LoggerModule],
  providers: [ResponseInterceptorProvider],
})
export class AppModule {}
