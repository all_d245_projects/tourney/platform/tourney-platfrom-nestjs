import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { passportJwtSecret } from 'jwks-rsa';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(configService: ConfigService) {
    const auth0Audience = configService.get('auth.auth0.audience');
    const auth0IssuerUrl = configService.get('auth.auth0.issuerUrl');

    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: configService.get('auth.auth0.cache'),
        rateLimit: configService.get('auth.auth0.rateLimit'),
        jwksRequestsPerMinute: configService.get('auth.auth0.jwksRequestsPerMinute'),
        jwksUri: `${auth0IssuerUrl}.well-known/jwks.json`,
      }),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      audience: auth0Audience,
      issuer: auth0IssuerUrl,
      algorithms: ['RS256'],
    });
  }

  validate(payload: unknown): unknown {
    return payload;
  }
}
