import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from '@tourney-platform/gateway-service/app/security/auth0/jwt.strategy';
import authConfig from '@tourney-platform/gateway-service/config/registered/auth.config';

@Module({
  imports: [ConfigModule.forFeature(authConfig), PassportModule.register({ defaultStrategy: 'jwt' })],
  providers: [ConfigService, JwtStrategy],
  exports: [PassportModule],
})
export class Auth0Module {}
