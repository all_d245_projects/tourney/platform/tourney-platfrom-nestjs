import { Logger, ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from '@tourney-platform/gateway-service/app/app.module';
import { RpcExceptionToHttpExceptionFilter } from '@tourney-platform/gateway-service/app/common/filter/rpc-exception.filter';
import { LoggerService } from '@tourney-platform/gateway-service/app/modules/logger/services/logger.service';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  const configService = app.get(ConfigService);
  const httpHost = configService.get('http.host');
  const httpPort = configService.get('http.port');
  const swaggerBuilder = configService.get('app.swagger.builder');
  const swaggerOptions = configService.get('app.swagger.options');
  const document = SwaggerModule.createDocument(app, swaggerBuilder, swaggerOptions);

  app.useLogger(app.get(LoggerService));

  app.use(helmet());

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: '1',
  });

  SwaggerModule.setup('swagger', app, document);

  app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

  app.useGlobalFilters(new RpcExceptionToHttpExceptionFilter());

  await app.listen(httpPort);

  Logger.log(`Gateway service HTTP API ready and listening on ${httpHost}:${httpPort}`, 'AppBootstrap');
  Logger.log('GATEWAY-SERVICE ready...', 'AppBootstrap');
}
bootstrap();
