# Gateway Service - NestJS

## Description
Public facing service to broker communication from clients to the respective microservice.

## Installation
1. Install dependencies
    ```bash
    npm install
    ```
2. Copy environment variable files
    ```bash
    cp .env.sample .env && cp sample.env .env.test
    ```
<br/>

## Configuration
The project is configured in two primary locations:

**.env file**
The `.env` file located at the root of this directory (after copying the `.env.example` file) and it stores configurations which can be modified per each environment.
The available configurations are defined below: 

| Environment Variable | Default value                                | Description                                               |
|----------------------|----------------------------------------------|-----------------------------------------------------------|
| **NODE_ENV**         | `local`                                      | Node environment: valid options include [`local`, `test`] |
| **HTTP_HOST**        | `localhost`                                  | HTTP host of the API                                      |
| **HTTP_PORT**        | `3009`                                       | HTTP port of the API                                      |
| **NATS_HOST**        | `localhost`                                  | NATS host for connecting to the NATS service              |
| **NATS_PORT**        | `4222`                                       | NATS port for connecting to the NATS service              |
| **AUTH0_ISSUER_URL** | `https://tourney-platform-dev.eu.auth0.com/` | Auth0 API application issuer url                          |
| **AUTH0_AUDIENCE**   | `gateway-service-nestjs`                     | Auth0 API audience                                        |


**configuration files**
The configuration files are Nest.js configurations which modify certain aspects of the application.
These files are located in the direction `src/config/registered`.
The following files can be modified:
- `src/config/registered/app.config.ts`: This file includes various app-wide configurations such as environment, global validation behaviour, etc.
- `src/config/registered/http.config.ts`: This file includes http related configurations.
- `src/config/registered/nats.config.ts`: This file includes NATS related configurations.
- `src/config/registered/auth0.config.ts`: This file includes Auth0 related configurations.

## Running the app

```bash
# start application in watch mode using local environment
$ npm run start:local

# start application via a REPL environment (useful for testing services)
$ npm run start:repl
```

<br/>

## Test
```bash
# unit tests
npm run test:unit

# unit test coverage
npm run test:unit:cov

# unit test pipeline
npm run test:unit:ci

# e2e tests
npm run test:e2e

# e2e tests coverage
npm run test:e2e:cov

# e2e tests pipeline
npm run test:e2e:ci
```

## Swagger Documentation
The configuration of the swagger documentation can be found [here](../../tools/swagger-doc/README.md).
Execute the generation of the swagger documentation by running `npm run openapi:spec-generate`.

## CI/CD Pipeline
The pipeline job for this microservice will be added to the merge request pipeline when files related to this microservice have been modified.
The jobs for this microservice include:
- build the gate-way service via `nest build`
- prepare the .env file
- execute unit tests
- execute e2e tests
- generate swagger documentation
