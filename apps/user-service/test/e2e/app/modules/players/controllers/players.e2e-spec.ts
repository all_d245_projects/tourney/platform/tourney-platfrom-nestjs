import { TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/user-service/app/common/services/prisma/prisma.service';
import { AppTestHelper } from '@tourney-platform/user-service/test/helpers/app-test.helper';
import { PlayerTestHelper } from '@tourney-platform/user-service/test/helpers/player-test.helper';
import { catchError, Observable, of } from 'rxjs';

describe('PlayersController (e2e)', () => {
  let appTestHelper: AppTestHelper;
  let prisma: PrismaService;

  beforeEach(async () => {
    appTestHelper = new AppTestHelper();

    const testingModule: TestingModule = await AppTestHelper.createE2eTestingModule();

    await appTestHelper.init(testingModule);

    prisma = appTestHelper.getApp().get(PrismaService);
    await prisma.player.deleteMany();
  });

  afterEach(async () => {
    await appTestHelper.terminate();
  });

  describe('get-players (Handler)', () => {
    it('returns players response', done => {
      prisma.player
        .createMany({
          data: PlayerTestHelper.getMockPlayerData(5),
        })
        .then(() => {
          const response: Observable<any> = appTestHelper.getNatsClient().send('get-players', {});

          response.subscribe(response => {
            expect(response.message).toEqual('get-players OK');
            expect(response).toHaveProperty('result');
            expect(response.result).toHaveLength(5);

            response.result.forEach(player => {
              expect(player).toHaveProperty('id');
              expect(player).toHaveProperty('playerProfile.email');
              expect(player).toHaveProperty('playerProfile.username');
              expect(player).toHaveProperty('playerProfile.firstName');
              expect(player).toHaveProperty('playerProfile.lastName');
              expect(player).toHaveProperty('playerIdentityProvider.provider');
              expect(player).toHaveProperty('playerIdentityProvider.providerId');
            });

            done();
          });
        })
        .catch(err => {
          throw err;
        });
    });
  });

  describe('create-player (Handler)', () => {
    it('returns player after creating it', done => {
      const playerCreateData = {
        playerProfile: {
          email: 'player_email@domain.com',
          username: 'firstname_lastName',
          firstName: 'firstname',
          lastName: 'lastname',
        },
        playerIdentityProvider: {
          provider: 'AUTH0',
          providerId: 'some-auth0-id',
        },
      };

      const response: Observable<any> = appTestHelper.getNatsClient().send('register-player', playerCreateData);

      response.subscribe(response => {
        expect(response.message).toEqual('register-player OK');
        expect(response).toHaveProperty('result');
        expect(response.result).toHaveProperty('id');
        expect(response.result.playerProfile.email).toEqual(playerCreateData.playerProfile.email);
        expect(response.result.playerProfile.username).toEqual(playerCreateData.playerProfile.username);
        expect(response.result.playerProfile.firstName).toEqual(playerCreateData.playerProfile.firstName);
        expect(response.result.playerProfile.lastName).toEqual(playerCreateData.playerProfile.lastName);
        expect(response.result.playerIdentityProvider.provider).toEqual(
          playerCreateData.playerIdentityProvider.provider,
        );
        expect(response.result.playerIdentityProvider.providerId).toEqual(
          playerCreateData.playerIdentityProvider.providerId,
        );

        done();
      });
    });

    it('returns a validation error when passed invalid post message', done => {
      // email property is missing
      const playerCreateData = {
        playerProfile: {
          username: 'firstname_lastName',
          firstName: 'firstname',
          lastName: 'lastname',
        },
      };

      const response: Observable<any> = appTestHelper
        .getNatsClient()
        .send('register-player', playerCreateData)
        .pipe(catchError(error => of(error)));

      response.subscribe(response => {
        expect(response.message).toEqual('Validation Error');
        expect(response.result[0]).toEqual(
          'property playerProfile.email has failed the following constraints: email must be an email',
        );

        done();
      });
    });
  });
});
