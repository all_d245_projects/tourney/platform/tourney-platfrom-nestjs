import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientProxy, ClientProxyFactory, MicroserviceOptions } from '@nestjs/microservices';
import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '@tourney-platform/user-service/app/app.module';
import { PrismaService } from '@tourney-platform/user-service/app/common/services/prisma/prisma.service';

export class AppTestHelper {
  private app: INestApplication;
  private natsClient: ClientProxy;
  private prisma: PrismaService;

  public async init(testingModule: TestingModule) {
    this.app = this.createApplication(testingModule);
    await this.app.startAllMicroservices();
    await this.app.init();

    this.natsClient = this.app.get('NATS_SERVICE');
    this.prisma = this.app.get(PrismaService);

    await this.natsClient.connect();
  }

  public getApp(): INestApplication {
    return this.app;
  }

  public getNatsClient(): ClientProxy {
    return this.natsClient;
  }

  public getPrismaService(): PrismaService {
    return this.prisma;
  }

  public async terminate() {
    await this.prisma.$disconnect();
    await this.app.close();
    this.natsClient.close();
  }

  static async createE2eTestingModule(): Promise<TestingModule> {
    return await Test.createTestingModule({
      imports: [AppModule],
      providers: [
        {
          provide: 'NATS_SERVICE',
          useFactory: (configService: ConfigService) => {
            const userServiceNatsConfig = configService.get('nats.userService');

            return ClientProxyFactory.create(userServiceNatsConfig);
          },
          inject: [ConfigService],
        },
      ],
    }).compile();
  }

  private createApplication(testingModule: TestingModule): INestApplication {
    const app: INestApplication = testingModule.createNestApplication();
    const configService: ConfigService = testingModule.get<ConfigService>(ConfigService);
    const userServiceNatsConfig = configService.get('nats.userService');

    app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

    app.connectMicroservice<MicroserviceOptions>(userServiceNatsConfig, {
      inheritAppConfig: true,
    });

    return app;
  }
}
