import { faker } from '@faker-js/faker';
import { IdentiyProvider, Player } from '@tourney-platform/user-service/prisma/client';
import { UniqueEnforcer } from 'enforce-unique';

export class PlayerTestHelper {
  static getMockPlayers(numberOfPlayers = 5): Player[] {
    const players: Player[] = [];
    const uniqueEnforcerEmail: UniqueEnforcer = new UniqueEnforcer();

    for (let i = 0; i < numberOfPlayers; i++) {
      const player = this.getRawPlayerData(uniqueEnforcerEmail);

      players.push(player as Player);
    }

    return players;
  }

  static getMockPlayerData(numberOfPlayers = 5): any[] {
    const players: any[] = [];
    const uniqueEnforcerEmail: UniqueEnforcer = new UniqueEnforcer();

    for (let i = 0; i < numberOfPlayers; i++) {
      const player = this.getRawPlayerData(uniqueEnforcerEmail);
      delete player.id;

      players.push(player);
    }

    return players;
  }

  private static getRawPlayerData(uniqueEnforcerEmail: UniqueEnforcer): Record<string, unknown> {
    const sex = faker.person.sexType();
    const firstName = faker.person.firstName(sex);
    const lastName = faker.person.lastName();
    const username = faker.internet.userName({ firstName, lastName });
    const email = uniqueEnforcerEmail.enforce(() => {
      return faker.internet.email({ firstName, lastName });
    });

    const player = {
      id: faker.string.uuid(),
      playerProfile: {
        email,
        username,
        firstName,
        lastName,
      },
      playerIdentityProvider: {
        provider: faker.helpers.enumValue(IdentiyProvider),
        providerId: faker.string.alphanumeric(8),
        providerData: {
          first: 'data',
          second: 'data',
          third: 'data',
        },
      },
    };

    return player;
  }
}
