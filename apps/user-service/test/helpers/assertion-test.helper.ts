import { ControllerResponseDto } from '@tourney-platform/user-service/app/common/dto/response.dto';

export class AssertionTestHelper {
  static isValidResponse(res: any) {
    expect(res).toMatchObject(new ControllerResponseDto());
  }

  static isValidUnauthorizedResponse(res: any) {
    expect(res.status).toEqual(401);
    expect(res.body).toStrictEqual({
      statusCode: 401,
      message: 'Unauthorized',
    });
  }
}
