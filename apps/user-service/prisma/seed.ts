import { PrismaClient } from '@tourney-platform/user-service/prisma/client';

import { players } from './fixtures/players';

const prisma = new PrismaClient();

const seed = async () => {
  try {
    await prisma.player.deleteMany();
    console.log('Deleted records in player collection');

    await prisma.player.createMany({
      data: players,
    });

    console.log('Added player data');
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    await prisma.$disconnect();
  }
};

seed();
