import { IdentiyProvider } from '@tourney-platform/user-service/prisma/client';

export const players = [
  {
    playerProfile: {
      email: 'player1@domain.com',
      username: 'player1-username',
      firstName: 'player',
      lastName: 'one',
    },
    playerIdentityProvider: {
      provider: IdentiyProvider.AUTH0,
      providerId: 'auth0_id_1',
      providerData: {},
    },
  },
  {
    playerProfile: {
      email: 'player2@domain.com',
      username: 'player2-username',
      firstName: 'player',
      lastName: 'one',
    },
    playerIdentityProvider: {
      provider: IdentiyProvider.AUTH0,
      providerId: 'auth0_id_2',
      providerData: {},
    },
  },
  {
    playerProfile: {
      email: 'player3@domain.com',
      username: 'player2-username',
      firstName: 'player',
      lastName: 'one',
    },
    playerIdentityProvider: {
      provider: IdentiyProvider.AUTH0,
      providerId: 'auth0_id_3',
      providerData: {},
    },
  },
];
