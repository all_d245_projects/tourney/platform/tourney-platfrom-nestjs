import { registerAs } from '@nestjs/config';
import { RpcException } from '@nestjs/microservices';
import { Environment } from '@tourney-platform/user-service/app/common/validation/environmentVariable.validation';
import { ValidationError } from 'class-validator';

export default registerAs('app', () => ({
  environment: process.env.NODE_ENV,
  globalValidationPipeOptions: {
    exceptionFactory: (validationErrors: ValidationError[]) => {
      const errors: string[] = [];

      validationErrors.forEach(error => {
        error
          .toString(false, true, '', true)
          .split('\n')
          .forEach(val => {
            if (val !== '') {
              errors.push(val.slice(3).trim());
            }
          });
      });

      throw new RpcException({
        message: 'Validation Error',
        result: errors,
      });
    },
    whitelist: true,
    forbidNonWhitelisted: true,
    forbidUnknownValues: true,
    transform: true,
    disableErrorMessages: process.env.NODE_ENV === Environment.Production,
    validationError: {
      value: true,
      target: true,
    },
  },
}));
