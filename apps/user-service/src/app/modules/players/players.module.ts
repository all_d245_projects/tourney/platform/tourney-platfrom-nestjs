import { Module } from '@nestjs/common';
import { PrismaService } from '@tourney-platform/user-service/app/common/services/prisma/prisma.service';
import { PlayersController } from '@tourney-platform/user-service/app/modules/players/controllers/players.controller';
import { PlayersService } from '@tourney-platform/user-service/app/modules/players/services/players.service';

@Module({
  providers: [PlayersService, PrismaService],
  controllers: [PlayersController],
})
export class PlayersModule {}
