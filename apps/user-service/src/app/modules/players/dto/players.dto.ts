import { GetCollectionDto } from '@tourney-platform/user-service/app/common/dto/get-collection.dto';
import { IdentiyProvider, Prisma } from '@tourney-platform/user-service/prisma/client';
import { Type } from 'class-transformer';
import { IsEmail, IsEnum, IsObject, IsOptional, IsString, ValidateNested } from 'class-validator';

export class GetPlayersDTO extends GetCollectionDto {}

class PlayerProfileDto {
  @IsEmail()
  email: string;

  @IsString()
  username: string;

  @IsOptional()
  @IsString()
  firstName?: string;

  @IsOptional()
  @IsString()
  lastName?: string;
}

class PlayerIdentityProviderDto {
  @IsEnum(IdentiyProvider)
  provider: IdentiyProvider;

  @IsString()
  providerId: string;

  @IsObject()
  @IsOptional()
  providerData?: Prisma.JsonValue | null;
}

export class CreatePlayerDTO {
  @ValidateNested()
  @Type(() => PlayerProfileDto)
  playerProfile: PlayerProfileDto;

  @ValidateNested()
  @Type(() => PlayerIdentityProviderDto)
  @IsOptional()
  playerIdentityProvider?: PlayerIdentityProviderDto;
}
