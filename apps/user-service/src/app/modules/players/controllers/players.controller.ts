import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ControllerResponseDto } from '@tourney-platform/user-service/app/common/dto/response.dto';
import { CreatePlayerDTO } from '@tourney-platform/user-service/app/modules/players/dto/players.dto';
import { PlayersService } from '@tourney-platform/user-service/app/modules/players/services/players.service';

export enum PlayersResponseMessage {
  GET_PLAYERS_OK = 'get-players OK',
  REGISTER_PLAYER_OK = 'register-player OK',
}

@Controller()
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @MessagePattern('get-players')
  async getPlayers(): Promise<ControllerResponseDto> {
    const players = await this.playersService.getPlayers();

    return {
      message: PlayersResponseMessage.GET_PLAYERS_OK,
      result: players,
    };
  }

  @MessagePattern('register-player')
  async registerPlayer(@Payload() data: CreatePlayerDTO): Promise<ControllerResponseDto> {
    const player = await this.playersService.createPlayer(data);

    return {
      message: PlayersResponseMessage.REGISTER_PLAYER_OK,
      result: player,
    };
  }
}
