import { Injectable } from '@nestjs/common';
import { DatabaseFailureHandler } from '@tourney-platform/user-service/app/common/decorators/database-exception.decorator';
import { PrismaService } from '@tourney-platform/user-service/app/common/services/prisma/prisma.service';
import { CreatePlayerDTO } from '@tourney-platform/user-service/app/modules/players/dto/players.dto';
import { Player } from '@tourney-platform/user-service/prisma/client';

@DatabaseFailureHandler
@Injectable()
export class PlayersService {
  constructor(private prismaService: PrismaService) {}

  async getPlayers(): Promise<Player[]> {
    return await this.prismaService.player.findMany();
  }

  async createPlayer(playerData: CreatePlayerDTO): Promise<Player> {
    return await this.prismaService.player.create({
      data: playerData,
    });
  }
}
