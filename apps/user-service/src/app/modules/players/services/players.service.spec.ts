import { Test, TestingModule } from '@nestjs/testing';
import { PrismaService } from '@tourney-platform/user-service/app/common/services/prisma/prisma.service';
import { CreatePlayerDTO } from '@tourney-platform/user-service/app/modules/players/dto/players.dto';
import { PlayersService } from '@tourney-platform/user-service/app/modules/players/services/players.service';
import { PrismaClient } from '@tourney-platform/user-service/prisma/client';
import { PlayerTestHelper } from '@tourney-platform/user-service/test/helpers/player-test.helper';
import { plainToInstance } from 'class-transformer';
import { DeepMockProxy, mockDeep } from 'jest-mock-extended';

describe('PlayersService', () => {
  let playersService: PlayersService;
  let prisma: DeepMockProxy<PrismaClient>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlayersService, PrismaService],
    })
      .overrideProvider(PrismaService)
      .useValue(mockDeep<PrismaClient>())
      .compile();

    playersService = module.get(PlayersService);
    prisma = module.get(PrismaService);
  });

  it('should be defined', () => {
    expect(playersService).toBeDefined();
  });

  describe('getPlayers', () => {
    it('should return an array of players', async () => {
      const expectedResult = PlayerTestHelper.getMockPlayers(5);

      prisma.player.findMany.mockResolvedValueOnce(expectedResult);

      expect(playersService.getPlayers()).resolves.toBe(expectedResult);
    });
  });

  describe('registerPlayer', () => {
    it('should return a single player', async () => {
      const expectedResult = PlayerTestHelper.getMockPlayers(1)[0];
      const playerCreateDto = plainToInstance(CreatePlayerDTO, {
        playerProfile: expectedResult.playerProfile,
        playerIdentityProvider: expectedResult.playerIdentityProvider,
      });

      prisma.player.create.mockResolvedValueOnce(expectedResult);

      expect(playersService.createPlayer(playerCreateDto)).resolves.toBe(expectedResult);
    });
  });
});
