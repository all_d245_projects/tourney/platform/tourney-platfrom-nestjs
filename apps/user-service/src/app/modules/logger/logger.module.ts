import { Module } from '@nestjs/common';
import { LoggerService } from '@tourney-platform/user-service/app/modules/logger/services/logger.service';

@Module({
  providers: [LoggerService],
  exports: [LoggerService],
})
export class LoggerModule {}
