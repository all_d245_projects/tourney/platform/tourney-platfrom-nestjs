import { Module } from '@nestjs/common';
import { ResponseInterceptorProvider } from '@tourney-platform/user-service/app/common/interceptors/response.interceptor';
import { LoggerModule } from '@tourney-platform/user-service/app/modules/logger/logger.module';
import { NatsClientModule } from '@tourney-platform/user-service/app/modules/nats/nats-client.module';
import { PlayersModule } from '@tourney-platform/user-service/app/modules/players/players.module';
import ConfigModuleConfig from '@tourney-platform/user-service/config/modules/config-module.config';

@Module({
  imports: [ConfigModuleConfig, PlayersModule, NatsClientModule, LoggerModule],
  providers: [ResponseInterceptorProvider],
})
export class AppModule {}
