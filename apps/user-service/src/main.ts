import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from '@tourney-platform/user-service/app/app.module';
import { LoggerService } from '@tourney-platform/user-service/app/modules/logger/services/logger.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
  });
  const configService = app.get(ConfigService);
  const natsHost = configService.get('nats.host');
  const natsPort = configService.get('nats.port');
  const userServiceNatsConfig = configService.get('nats.userService');

  app.useLogger(app.get(LoggerService));

  app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

  app.connectMicroservice<MicroserviceOptions>(userServiceNatsConfig, {
    inheritAppConfig: true,
  });

  await app.startAllMicroservices();
  Logger.log(`NATS ready and listening on ${natsHost}:${natsPort}`, 'AppBootstrap');
  Logger.log('USER-SERVICE ready...', 'AppBootstrap');
}
bootstrap();
