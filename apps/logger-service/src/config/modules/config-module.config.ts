import { ConfigModule } from '@nestjs/config';
import { Environment } from '@tourney-platform/logger-service/app/common/validation/environmentVariable.validation';
import { validate } from '@tourney-platform/logger-service/app/common/validation/environmentVariable.validation';
import appConfig from '@tourney-platform/logger-service/config/registered/app.config';
import natsConfig from '@tourney-platform/logger-service/config/registered/nats.config';

function getConfigOptionsFromEnvironmentContext(): Record<string, unknown> {
  switch (process.env.NODE_ENV) {
    case Environment.Production:
    case Environment.Development:
    case Environment.Local:
      return {
        cache: true,
        envFilePath: '.env',
      };

    case Environment.Test:
      return {
        cache: false,
        envFilePath: '.env.test',
      };

    default:
      throw new Error('could not read NODE_ENV from the environment.');
  }
}

export default ConfigModule.forRoot({
  isGlobal: true,
  load: [natsConfig, appConfig],
  validate,
  ...getConfigOptionsFromEnvironmentContext(),
});
