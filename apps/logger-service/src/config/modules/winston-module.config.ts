import 'winston-daily-rotate-file';

import { WinstonLogger } from '@tourney-platform/logger-service/app/modules/logger/services/winston-logger.service';
import { utilities as nestWinstonModuleUtilities, WinstonModule } from 'nest-winston';
import * as path from 'path';
import * as process from 'process';
import { format as winstonFormat, transports } from 'winston';

const rootPath = process.cwd();
const logPath = path.join(rootPath, 'logs');
const config = {
  paths: {
    log: logPath,
    loggerService: path.join(logPath, 'services/logger-service'),
    gatewayService: path.join(logPath, 'services/gateway-service'),
    userService: path.join(logPath, 'services/user-service'),
    coreService: path.join(logPath, 'services/core-service'),
  },
};

const getLoglevel = () => {
  const env = process.env.NODE_ENV || 'development';
  const isDevelopment = env === 'development' || env === 'local';
  return isDevelopment ? 'debug' : 'warn';
};

const getDateFormatted = () => {
  const d = new Date();
  const year = d.getFullYear();
  let month = '' + (d.getMonth() + 1);
  let day = '' + d.getDate();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
};

const createLoggerOptions = (directory: string, fileName: string, level = 'debug', additionalOptions?: object) => {
  const options = {
    level,
    format: winstonFormat.combine(winstonFormat.json()),
    dirname: `${directory}/${getDateFormatted()}`,
    filename: path.resolve(`${fileName}-%DATE%.log`),
    frequency: '1d',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxFiles: '30d',
    options: {
      flag: 'r+',
      colorize: false,
    },
  };

  return additionalOptions ? { ...options, ...additionalOptions } : options;
};

const format = winstonFormat.combine(winstonFormat.printf(info => `${info.timestamp} ${info.level}: ${info.message}`));

const loggerServiceTransports = [
  // console logs
  new transports.Console({
    format: winstonFormat.combine(
      winstonFormat.timestamp(),
      winstonFormat.ms(),
      nestWinstonModuleUtilities.format.nestLike(),
    ),
  }),
  // error only logs
  new transports.DailyRotateFile(
    createLoggerOptions(config.paths.loggerService, 'app-error', 'error', {
      format: winstonFormat.combine(winstonFormat.timestamp(), winstonFormat.json()),
    }),
  ),
  // all logs
  new transports.DailyRotateFile(
    createLoggerOptions(config.paths.loggerService, 'app-all', 'debug', {
      format: winstonFormat.combine(winstonFormat.timestamp(), winstonFormat.json()),
    }),
  ),
];

const gatewayServiceTransports = [
  // error only logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.gatewayService, 'app-error', 'error')),
  // all logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.gatewayService, 'app-all')),
];

const userServiceTransports = [
  // error only logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.userService, 'app-error', 'error')),
  // all logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.userService, 'app-all')),
];

const coreServiceTransports = [
  // error only logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.coreService, 'app-error', 'error')),
  // all logs
  new transports.DailyRotateFile(createLoggerOptions(config.paths.coreService, 'app-all')),
];

export const AppLogger = WinstonModule.createLogger({
  level: getLoglevel(),
  transports: loggerServiceTransports,
  format,
  exitOnError: false,
});

export const GatewayServiceLoggerProvider = {
  provide: 'GATEWAY_SERVICE_LOGGER',
  useValue: new WinstonLogger({
    level: getLoglevel(),
    transports: gatewayServiceTransports,
    format,
    exitOnError: false,
  }),
};

export const UserServiceLoggerProvider = {
  provide: 'USER_SERVICE_LOGGER',
  useValue: new WinstonLogger({
    level: getLoglevel(),
    transports: userServiceTransports,
    format,
    exitOnError: false,
  }),
};

export const CoreServiceLoggerProvider = {
  provide: 'CORE_SERVICE_LOGGER',
  useValue: new WinstonLogger({
    level: getLoglevel(),
    transports: coreServiceTransports,
    format,
    exitOnError: false,
  }),
};
