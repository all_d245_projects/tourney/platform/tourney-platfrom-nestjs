import { Catch, ExceptionFilter } from '@nestjs/common';
import { Logger } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: any) {
    switch (true) {
      case exception instanceof RpcException:
        const { message, result } = exception.getError();

        Logger.error(`${message}: ${result}`);
        break;

      default:
        Logger.error(String(exception.getMessage()));
        break;
    }
  }
}
