import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { MicroserviceResposne } from '@tourney-platform/logger-service/app/common/types/microservice.type';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class MicroserviceResponseInterceptor<T> implements NestInterceptor<T, MicroserviceResposne> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<MicroserviceResposne> {
    return next.handle().pipe(
      map(data => ({
        message: data?.message || 'ACK',
        result: data?.result || {},
      })),
    );
  }
}

export const MicroserviceResponseInterceptorProvider = {
  provide: APP_INTERCEPTOR,
  useClass: MicroserviceResponseInterceptor,
};
