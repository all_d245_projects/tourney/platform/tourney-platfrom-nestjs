import { Module } from '@nestjs/common';
import { MicroserviceLoggerService } from '@tourney-platform/logger-service/app/modules/logger/services/microservice-logger.service';
import {
  CoreServiceLoggerProvider,
  GatewayServiceLoggerProvider,
  UserServiceLoggerProvider,
} from '@tourney-platform/logger-service/config/modules/winston-module.config';

import { MicroserviceLoggerController } from './controllers/microservice-logger.controller';

@Module({
  providers: [
    GatewayServiceLoggerProvider,
    UserServiceLoggerProvider,
    CoreServiceLoggerProvider,
    MicroserviceLoggerService,
  ],
  exports: [GatewayServiceLoggerProvider, UserServiceLoggerProvider, CoreServiceLoggerProvider],
  controllers: [MicroserviceLoggerController],
})
export class LoggerModule {}
