import { Injectable, LoggerService } from '@nestjs/common';
import { createLogger, Logger } from 'winston';

@Injectable()
export class WinstonLogger implements LoggerService {
  private logger: Logger;

  constructor(private loggerOptions: object) {
    this.logger = createLogger(loggerOptions);
  }

  log(level: string, timestamp: string, message: string, meta: any) {
    this.logger.log(level, message, { ...meta, timestamp });
  }

  error(level: string, timestamp: string, message: string, meta: any) {
    this.logger.error(message, { ...meta, timestamp });
  }

  warn(level: string, timestamp: string, message: string, meta: any) {
    this.logger.error(message, { ...meta, timestamp });
  }
}
