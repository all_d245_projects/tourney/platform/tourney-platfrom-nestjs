import {
  Microservice,
  MicroserviceLogLevel,
} from '@tourney-platform/logger-service/app/modules/logger/services/microservice-logger.service';
import { IsEnum, IsISO8601, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateMicroserviceLogDto {
  @IsNotEmpty()
  @IsEnum(Microservice)
  service: Microservice;

  @IsEnum(MicroserviceLogLevel)
  @IsNotEmpty()
  logLevel: MicroserviceLogLevel;

  @IsISO8601({ strict: true, strictSeparator: true })
  @IsNotEmpty()
  timestamp: string;

  @IsString()
  @IsNotEmpty()
  message: string;

  @IsOptional()
  meta?: any;
}
