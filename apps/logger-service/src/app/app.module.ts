import { Module } from '@nestjs/common';
import { MicroserviceResponseInterceptor } from '@tourney-platform/logger-service/app/common/interceptors/microservice-response.interceptor';
import { LoggerModule } from '@tourney-platform/logger-service/app/modules/logger/logger.module';
import ConfigModuleConfig from '@tourney-platform/logger-service/config/modules/config-module.config';

@Module({
  imports: [ConfigModuleConfig, LoggerModule],
  providers: [MicroserviceResponseInterceptor],
})
export class AppModule {}
