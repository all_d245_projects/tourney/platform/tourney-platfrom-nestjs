import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from '@tourney-platform/logger-service/app/app.module';
import { AppLogger } from '@tourney-platform/logger-service/config/modules/winston-module.config';

import { AllExceptionsFilter } from './app/common/filter/all-exceptions-filter.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    bufferLogs: true,
    logger: AppLogger,
  });
  const configService = app.get(ConfigService);
  const natsLoggerServiceConfig = configService.get('nats.loggerService');

  app.useGlobalPipes(new ValidationPipe(configService.get('app.globalValidationPipeOptions')));

  app.useGlobalFilters(new AllExceptionsFilter());

  app.connectMicroservice<MicroserviceOptions>(natsLoggerServiceConfig, {
    inheritAppConfig: true,
  });

  await app.startAllMicroservices();
  Logger.log(`NATS ready and listening on ${natsLoggerServiceConfig.options.servers}`, 'AppBootstrap');
  Logger.log('LOGGER-SERVICE ready...', 'AppBootstrap');
}
bootstrap();
