import { repl } from '@nestjs/core';
import { AppModule } from '@tourney-platform/logger-service/app/app.module';

async function bootstrap() {
  await repl(AppModule);
}
bootstrap();
