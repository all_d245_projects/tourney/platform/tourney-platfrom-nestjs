# Docker Services

## Description
Docker compose file managing the various services required for the Tourney platform during local development.

<br/>

## Services

### [Nats](https://nats.io)  
A NATS server to facilitate communication between the microservices

### [MongoDB](https://www.mongodb.com)
MongoDB cluster as a replica set to support transactions.  
The mongo replica set cluster needs a replica key file. You can generate one by running:
```bash
openssl rand -base64 756 > files/mongo/replica.key
sudo chmod 400 files/mongo/replica.key
```
The MongoDB will have the following databases:
  - **user-service-dev**: For the development user service microservice.
  - **user-service-test**: For the test user service microservice.
  - **core-service-dev**: For the development core service microservice.
  - **core-service-test**: For the test core service microservice.

<br/>

## Installation
1. Ensure [Docker & Docker Compose](https://docs.docker.com/engine/install/) are installed locally.
2. Generate a [mongoDB replica key file](https://www.mongodb.com/docs/manual/tutorial/deploy-replica-set-with-keyfile-access-control/#create-a-keyfile):
   ```bash
   openssl rand -base64 756 > files/mongo/replica.key
   sudo chmod 400 files/mongo/replica.key
   ```
3. run the docker services: 
    ```bash
    docker compose up -d
    ```