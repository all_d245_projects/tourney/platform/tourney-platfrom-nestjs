#!/bin/bash

m1=mongo1
m2=mongo2
m3=mongo3
port=27017

echo "###### Waiting for ${m1} instance startup.."
until mongosh --host ${m1}:${port} --eval 'quit(db.runCommand({ ping: 1 }).ok ? 0 : 2)' &>/dev/null; do
  printf '.'
  sleep 1
done
echo "###### Working ${m1} instance found, initiating user setup & initializing rs setup.."

#initialize replica sets
mongosh mongodb://${m1}:${port}/admin?directConnection=true <<EOF
var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';

db.auth(rootUser, rootPassword);

var config = {
    "_id": "tourney-platform-rs",
    "version": 1,
    "members": [
        {
            "_id": 1,
            "host": "${m1}:${port}",
            "priority": 2
        },
        {
            "_id": 2,
            "host": "${m2}:${port}",
            "priority": 1
        },
        {
            "_id": 3,
            "host": "${m3}:${port}",
            "priority": 1,
            "arbiterOnly": true 
        }
    ]
};

rs.initiate(config, { force: true });
rs.status();
quit();
EOF

#initialize database users
mongosh mongodb://${m1}:${port}/admin?directConnection=false <<EOF
var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';

db.auth(rootUser, rootPassword);
db.getMongo().setReadPref('primary');

const databases = [
// user-service database & user for dev env
{
  username: "user-service-dev",
  password: "user-service-dev",
  roles: [{ role: "readWrite", db: "user-service-dev" }],
},

// user-service database & user for test env
{
  username: "user-service-test",
  password: "user-service-test",
  roles: [{ role: "readWrite", db: "user-service-test" }],
},

// core-service database & user for dev env
{
  username: "core-service-dev",
  password: "core-service-dev",
  roles: [{ role: "readWrite", db: "core-service-dev" }],
},

// core-service database & user for test env
{
  username: "core-service-test",
  password: "core-service-test",
  roles: [{ role: "readWrite", db: "core-service-test" }],
},
];

databases.forEach((elem) => {
  print("Upserting user and DB for " + elem.username);

  const user = db.getUser(elem.username);

  if (user) {
    db.updateUser(elem.username, {
      roles: elem.roles
    });

    print("User updated: " + elem.username);
  } else {
    db.createUser({
      user: elem.username,
      pwd: elem.password,
      roles: elem.roles
    });

    print("User created: " + elem.username);
  }

});

print("DONE!");

quit();
EOF