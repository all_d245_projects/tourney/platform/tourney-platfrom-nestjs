# Coverage Merge

## Description
Tool to merge the coverage test result of the various microservices.

## Configuration
The tool is a shell script that takes a comma separated list of microservices to merge as the first and only argument.
Add additional services as follows in the `exec` script configuration within the `package.json` file:
```bash
sh bin/coverage-merge.sh service-1,service-2,service-3
```

## Running Merge
Execute the merging tool by running `npm run test:merge` in the root of the repository.
**Note:** It is required the coverage test for the respective microservices be executed first.