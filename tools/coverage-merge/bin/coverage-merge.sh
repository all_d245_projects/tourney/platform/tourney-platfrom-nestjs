#!/bin/bash

projects="$(echo "$1" | tr -d '[:space:]')"
project_root="../.."
coverage_dir="${project_root}/coverage"
coverage_mereged_dir="${project_root}/coverage/merged"
temp_dir="${coverage_mereged_dir}/tmp"

# Check if the argument is empty
if [ -z "$projects" ]; then
    echo "Error: First argument is required to be a comma separated value of projects"
    exit 1  # Exit the script with a non-zero exit code to indicate an error
fi

# Convert comma-separated string to an array
projects_array=$(echo $projects | tr "," "\n")

# Check if the directory exists
if [ -d "$coverage_mereged_dir" ]; then
    rm -rf "$coverage_mereged_dir"
fi

# Create merged coveraage report directory
mkdir -p $coverage_mereged_dir
mkdir -p $temp_dir

# Loop through the projects
for project in $projects_array; do
  project_coverage_dir="${coverage_dir}/app/${project}"

  # check if unit & e2e tests have been created
  if [[ ! -f "${project_coverage_dir}/unit/coverage-final.json" && ! -f "${project_coverage_dir}/e2e/coverage-final.json" ]]; then
      echo "Unit & E2E coverage files not found for ${project}. skipping..."
      continue
  fi

  # copy unit & e2e json tests into project temp directory
  cp ${project_coverage_dir}/unit/coverage-final.json ${temp_dir}/${project}-unit-coverage-final.json
  cp ${project_coverage_dir}/e2e/coverage-final.json ${temp_dir}/${project}-e2e-coverage-final.json
done

echo "Generating report..."
npx nyc report -t ${temp_dir} --report-dir "${coverage_mereged_dir}/report" --reporter=lcov --reporter=text-summary --reporter=cobertura
rm -r ${temp_dir}