import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from '@tourney-platform/gateway-service/app/app.module';
import * as fs from 'fs';
import { ConfigService } from '@nestjs/config';
import swaggerDocConfig from '@tourney-platform/tools/swagger-doc/config/swagger-doc-config';

(async () => {
  try {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const configService = app.get(ConfigService);
    const swaggerBuilder = configService.get('app.swagger.builder');
    const swaggerOptions = configService.get('app.swagger.options');
    const document = SwaggerModule.createDocument(app, swaggerBuilder, swaggerOptions);

    fs.writeFileSync(
      `${swaggerDocConfig.outputDirectory}/${swaggerDocConfig.swaggerFileName}`,
      JSON.stringify(document),
    );

    await app.close();
  } catch (error) {
    console.error(error);
  } finally {
    process.exit(0);
  }
})();
