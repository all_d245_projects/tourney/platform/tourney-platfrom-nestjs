# Swagger Doc

## Description
Swagger API documentation generator.

## Configuration
The swagger doc generator is a module from Nest.js. The documentation on how to configure the Swagger Doc module can be found [here](https://docs.nestjs.com/openapi/introduction).
The configuration files for this package can be found in the config directory `tools/swagger-doc/config`

## Generating the Swagger Docs
The swagger docs can be executed by running the package.json script `exec`: 
```bash
npm run -w=@tools/swagger-doc exec
```
