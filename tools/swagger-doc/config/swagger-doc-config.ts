import { SwaggerDocumentOptions } from '@nestjs/swagger';
import path from 'path';

export default {
  outputDirectory: path.join(__dirname, '..', 'redoc'),
  swaggerFileName: 'tourney-platform-nestjs-gateway-service-openapi-spec.json',
  swaggerDocumentationOptions: {
    operationIdFactory: (methodKey: string) => methodKey,
  } as SwaggerDocumentOptions,
};
